import { writable, derived } from 'svelte/store';
import { getCookie, setCookie } from "../helpers/scripts/cookies";

const initialCookies = {
  "weUseCookies": true,
  "personalData": true,
};

export const prevStep = writable(0);
export const currentStep = writable(1);

export const step = derived(
  [prevStep, currentStep],
  ([$prevStep, $currentStep]) =>
    ({
      prev: $prevStep,
      current: $currentStep,
    })
);

export const appCookies = writable(initialCookies, set => {
  const weUseCookies = JSON.parse(getCookie('weUseCookies') || false);
  const personalData = JSON.parse(getCookie('personalData') || false);
  
  set({ ...initialCookies, weUseCookies, personalData });
});

export const hideNotification = writable(null);

/**
 * Mutators
 */

export function nextStep(value) {
  currentStep.update((step) => {
    prevStep.update(() => step);
    return value;
  })
}

export function backStep() {
  currentStep.update((step) => {
    let temp;
    prevStep.update((prev) => {
      temp = prev;
      return step;
    });
    
    return temp;
  })
}

/**
 * Subscriptions
 */

hideNotification.subscribe((key) => {
  if (!key) {
    return;
  }
  
  appCookies.update(() => ({ ...initialCookies, [key]: true }))
  setCookie(key, true, 365);
});
